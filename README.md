# libs

contains a maven repository to provide my libs that I don't think to be common enough to host it somewhere else

# usage

add the following lines to your `repositories` block in your `build.gradle`:

```groovy
maven {
  url "https://gitlab.com/jottyfan/libs/-/raw/main"
}
```

After that, you can add `implementation` `dependencies` the way as usual, but including libs from this repository.
